FROM debian:latest
RUN apt update -y
RUN apt install build-essential -y
ADD hola.cpp /
RUN g++ /hola.cpp
CMD /a.out
